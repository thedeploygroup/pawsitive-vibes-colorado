# Pawsitive Vibes Dog Training LLC

<img src="./ghi/src/images/PV_Logo.png" alt="Pawsitive Vibes Logo" width="800">

### Objective

This is the AWS deployment of a team project.
The project can be found at https://gitlab.com/pawsitive-vibes/pawsitive-vibes

## Project Team
-   Kyle McPeake
-   Austin Hamilton
-   James Keyser
-   Grace Lee
