"""
Entry point for the FastAPI Application
"""
from fastapi import FastAPI, Form, UploadFile
from fastapi.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware
from pathlib import Path
from routers import (
    auth_router,
    pets_router,
    users_router,
    services_router,
    testimonials_router,
    appointment_router,
    meetups_router,
)
import os

app = FastAPI()

allowed_origins = ["http://localhost:5173", "https://pawsitivevibescolorado.com", "https://pawsitive-vibes-colorado-thedeploygroup-3c8a5f079783bbde0465001.gitlab.io", "CORS_HOST", "18.224.245.26", "https://18.224.245.26"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=allowed_origins,
    # allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    max_age=1200,
)

app.include_router(auth_router.router)
app.include_router(users_router.router)
app.include_router(pets_router.router)
app.include_router(services_router.router)
app.include_router(appointment_router.router)
app.include_router(testimonials_router.router)
app.include_router(meetups_router.router)


UPLOAD_DIR = Path('static/profile_images')
SERVICE_IMAGES_DIR = Path('static/service_images')


@app.post('/upload/')
async def upload_file(file_upload: UploadFile, filename: str = Form(...)):
    data = await file_upload.read()
    save_to = UPLOAD_DIR / filename
    # delete old pic if it exists
    if save_to.is_file():
        os.remove(save_to)
    # add new profile pic with format user_id.png
    with open(save_to, 'wb') as f:
        f.write(data)

    return {'filename': filename}


@app.post('/upload_service_image/')
async def upload_service_image(
    file_upload: UploadFile, filename: str = Form(...)
):
    data = await file_upload.read()
    save_to = SERVICE_IMAGES_DIR / filename
    with open(save_to, 'wb') as f:
        f.write(data)
    return {'filename': filename}


@app.get('/profile_image/{id}')
async def get_profile_image(id: int):
    image_path = UPLOAD_DIR / f"{id}.png"
    if not image_path.is_file():
        return {"error": "Profile image not found"}

    return FileResponse(str(image_path), media_type='image/png')


@app.get('/service_image/{id}')
async def get_service_image(id: int):
    image_path = SERVICE_IMAGES_DIR / f"{id}.png"
    if not image_path.is_file():
        return {"error": "Service image not found"}
    return FileResponse(str(image_path), media_type='image/png')
