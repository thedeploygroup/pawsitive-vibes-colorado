import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
    base: '/',
    build: {
        outDir: 'dist',
        assetsDir: 'assets',
    },
    plugins: [react()],
    server: {
        host: true,
        strictPort: true,
        watch: {
            usePolling: true,
        },
    },
})
